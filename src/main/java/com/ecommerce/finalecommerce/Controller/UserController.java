package com.ecommerce.finalecommerce.Controller;

import com.ecommerce.finalecommerce.Entity.User;
import com.ecommerce.finalecommerce.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:3000")
public class UserController {

    @Autowired
    private UserService userService;
    @GetMapping("/getAllUsers")
    @Secured({"USER", "ADMIN"})
    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }
    @GetMapping("/basicauth")
    public String basicAuth(){
        return "Success";
    }
    @PostMapping("/addUser")
    @PreAuthorize("hasRole('ADMIN')")
    public User addUser(@RequestBody User user){
        return userService.addUser(user);
    }

    @PutMapping("/updateUser/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public User updateUser(@RequestBody User user, @PathVariable int id){
        return userService.updateUser(user, id);
    }
    @DeleteMapping("/deleteUser/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String deleteUser(@PathVariable int id){
        return userService.deleteUser(id);
    }
}
