package com.ecommerce.finalecommerce.Config;

import com.ecommerce.finalecommerce.Entity.Role;
import com.ecommerce.finalecommerce.Entity.User;
import com.ecommerce.finalecommerce.Repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class BasicAuthSecurityConfiguration {

    private final UserRepository userRepository;

    public BasicAuthSecurityConfiguration(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .authorizeHttpRequests(auth -> {
                    auth
                            .requestMatchers("/api/getAllUsers").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
//                  .requestMatchers("/api/getAllUsers").permitAll()
                            .requestMatchers("/basicauth").permitAll()
                            .requestMatchers("/api/addUser").hasRole("ADMIN")
                            .requestMatchers("/api/updateUser/{id}").hasRole("ADMIN")
                            .requestMatchers("/api/deleteUser/{id}").hasRole("ADMIN")
                            .anyRequest().authenticated();
                })
                .sessionManagement(session ->
                        session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .httpBasic(Customizer.withDefaults())
                .csrf(csrf -> csrf.disable());
        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return username -> {
            if (username.equals("admin")) {
                // Hardcoded admin user
                return org.springframework.security.core.userdetails.User.withUsername("admin")
                        .password(passwordEncoder().encode("admin"))
                        .roles(Role.ADMIN.name())
                        .build();
            } else {
                User user = userRepository.findByUsername(username);
                if (user != null) {
                    return org.springframework.security.core.userdetails.User.withUsername(user.getUsername())
                            .password(user.getPassword())
                            .roles(user.getRoles())
                            .build();
                }
                throw new UsernameNotFoundException("User not found with username: " + username);
            }
        };
    }

}
