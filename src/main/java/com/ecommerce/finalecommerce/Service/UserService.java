package com.ecommerce.finalecommerce.Service;

import com.ecommerce.finalecommerce.Entity.User;
import com.ecommerce.finalecommerce.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
    public User addUser(User user){
        user.setFirstName(user.getFirstName());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setUsername(user.getUsername());
        user.setLastName(user.getLastName());
        user.setEmail(user.getEmail());

        return userRepository.save(user);

    }
    //  Updating a student
    public User updateUser(User user, int id){
        User existingUser = userRepository.findById(id).orElse(null);
        existingUser.setFirstName(user.getFirstName());
        existingUser.setLastName(user.getLastName());
        existingUser.setEmail(user.getEmail());
        existingUser.setUsername(user.getUsername());
        return userRepository.save(existingUser);
    }
    //    Deleting a student
    public String deleteUser(int id){
        userRepository.deleteById(id);
        return "User Removed " + id;
    }
}
